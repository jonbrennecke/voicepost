import AVFoundation
import UIKit

class VideoAnimation {
    
    private var videoComposition = AVMutableVideoComposition()
    private var mixComposition = AVMutableComposition()
    private var videoAsset: AVURLAsset
    private var videoTrack: AVAssetTrack
//    private var audioTrack: AVAssetTrack
    private var effectLayer: CALayer
    private var videoLayer: CALayer
    private var parentLayer: CALayer
    
    init?(withVideoAtURL videoFileURL: URL) {
        videoAsset = AVURLAsset(url: videoFileURL, options: nil)
        guard let videoTrack = videoAsset.tracks(withMediaType: .video).first else {
            Debug.log(format: "Video track could not be found at url = %@", videoFileURL.absoluteString)
            return nil
        }
        self.videoTrack = videoTrack
//        guard let audioTrack = videoAsset.tracks(withMediaType: .audio).first else {
//            Debug.log(format: "Audio track could not be found at url = %@", videoFileURL.absoluteString)
//            return nil
//        }
//        self.audioTrack = audioTrack
        let originalSize = videoTrack.naturalSize
        let size = CGSize(width: originalSize.height, height: originalSize.width)
        let frame = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
        parentLayer = CALayer()
        parentLayer.frame = frame
        effectLayer = CALayer()
        effectLayer.frame = frame
        videoLayer = CALayer()
        videoLayer.frame = frame
        parentLayer.addSublayer(videoLayer)
        parentLayer.addSublayer(effectLayer)
    }
    
    public func addTextOverlay(withString string: String) -> VideoAnimation {
        let textContainerLayer = CALayer()
        let containerOffset: CGFloat = 100
        let containerHeight: CGFloat = 100
        let fontSize: CGFloat = 60
        let size = videoSize()
        textContainerLayer.frame = CGRect(x: 0, y: containerOffset, width: size.width, height: containerHeight)
        textContainerLayer.backgroundColor = ThemeColors.white.cgColor
        let textLayer = CATextLayer()
        let textOffset = -((containerHeight - fontSize) / 2 - fontSize / 10)
        textLayer.frame = CGRect(x: 0, y: textOffset, width: size.width, height: containerHeight)
        textLayer.string = string
        textLayer.foregroundColor = ThemeColors.darkPurple.cgColor
        textLayer.alignmentMode = .center
        textLayer.fontSize = fontSize
        textLayer.string = string
        textContainerLayer.addSublayer(textLayer)
        effectLayer.addSublayer(textContainerLayer)
        return self
    }
    
    public func exportVideo(callback: @escaping (Error?) -> ()) throws {
        try applyAnimationToVideo()
        DispatchQueue.global(qos: .background).async {
            do {
                let exportFileURL = try FileManager.default
                    .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                    .appendingPathComponent("output")
                    .appendingPathExtension("mov")
                try? FileManager.default.removeItem(at: exportFileURL)
                guard let assetExport = AVAssetExportSession(asset: self.mixComposition, presetName: AVAssetExportPresetHighestQuality) else {
                    Debug.log(message: "Asset export session could not be created")
                    return
                }
                assetExport.videoComposition = self.videoComposition
                assetExport.outputFileType = .mov
                assetExport.outputURL = exportFileURL
                Debug.log(format: "Exporting video animation. URL = %@", exportFileURL.absoluteString)
                assetExport.exportAsynchronously(completionHandler: {
                    UISaveVideoAtPathToSavedPhotosAlbum(exportFileURL.path, nil, nil, nil)
                    Debug.log(format: "Finished exporting video animation. URL = %@", exportFileURL.absoluteString)
                    callback(nil)
                })
            }
            catch let error {
                Debug.log(error: error)
                callback(error)
            }
        }
    }
    
    private func videoSize() -> CGSize {
        let originalSize = videoTrack.naturalSize
        return CGSize(width: originalSize.height, height: originalSize.width)
    }
    
    private func applyAnimationToVideo() throws {
        //        guard let mixCompositionAudioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid) else {
        //            Debug.log(message: "Unable to add audio track.")
        //            return
        //        }
        let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration)
        //        try mixCompositionAudioTrack.insertTimeRange(timeRange, of: audioTrack, at: CMTime.zero)
        guard let mixCompositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid) else {
            Debug.log(message: "Unable to add video track.")
            return
        }
        let videoTrack = videoAsset.tracks(withMediaType: .video).first!
        try mixCompositionVideoTrack.insertTimeRange(timeRange, of: videoTrack, at: CMTime.zero)
        mixCompositionVideoTrack.preferredTransform = videoTrack.preferredTransform
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: mixComposition.duration)
        guard let mixVideoTrack = mixComposition.tracks(withMediaType: .video).first else {
            Debug.log(message: "Video track could not be found in composition")
            return
        }
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: mixVideoTrack)
        layerInstruction.setTransform(videoTrack.preferredTransform, at: CMTime.zero)
        instruction.layerInstructions = [layerInstruction]
        videoComposition.instructions = [instruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30) // TODO check video fps
        videoComposition.renderSize = videoSize()
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
    }
}
