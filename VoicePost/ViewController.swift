import UIKit
import AVFoundation

class ViewController: UIViewController {

    private var transcriptionViewController = TranscriptionViewController()
    private var captureButtonViewController = CaptureButtonViewController()
    private var reviewViewController = ReviewViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTranscriptionViewController()
        setupCaptureButtonViewController()
        requestUserAuthorization() { success in
            guard success  else {
                Debug.log(message: "The user has not completed authorization of the app.")
                return
            }
            DispatchQueue.main.async {
                self.setupCamera()
                self.setupSpeechRecognition()
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let cameraManager = AppDelegate.sharedCameraManager
        guard cameraManager.isAuthorized() else {
            return
        }
        DispatchQueue.main.async {
            cameraManager.previewLayer.frame = self.view.bounds;
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupUI() {
        view.backgroundColor = ThemeColors.darkPurple
        setupGradients()
    }
    
    private func setupGradients() {
        let topGradientLayer = CAGradientLayer()
        topGradientLayer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 0.5 * view.frame.width)
        topGradientLayer.colors = [ThemeColors.darkPurple.withAlphaComponent(0.74).cgColor,
                                   ThemeColors.darkPurple.withAlphaComponent(0.0).cgColor]
        let bottomGradientLayer = CAGradientLayer()
        bottomGradientLayer.frame = CGRect(x: 0, y: 0.5 * view.frame.height, width: view.frame.width, height: 0.5 * view.frame.height)
        bottomGradientLayer.colors = [ThemeColors.darkPurple.withAlphaComponent(0.0).cgColor,
                                      ThemeColors.darkPurple.withAlphaComponent(0.74).cgColor]
        view.layer.addSublayer(topGradientLayer)
        view.layer.addSublayer(bottomGradientLayer)
    }
    
    private func requestUserAuthorization(callback: @escaping (Bool) -> ()) {
        let cameraManager = AppDelegate.sharedCameraManager
        cameraManager.authorize { success in
            guard success else {
                callback(false)
                return
            }
            let speechController = AppDelegate.sharedSpeechController
            speechController.authorize { success in
                guard success else {
                    callback(false)
                    return
                }
                callback(true)
            }
        }
    }
    
    private func setupCaptureButtonViewController() {
        addChild(captureButtonViewController)
        view.addSubview(captureButtonViewController.view)
        captureButtonViewController.didMove(toParent: self)
        captureButtonViewController.delegate = self
    }
    
    private func setupTranscriptionViewController() {
        addChild(transcriptionViewController)
        view.addSubview(transcriptionViewController.view)
        transcriptionViewController.didMove(toParent: self)
    }
    
    private func setupCamera() {
        let cameraManager = AppDelegate.sharedCameraManager
        cameraManager.delegate = self
        guard cameraManager.isAuthorized() else {
            return
        }
        self.view.layer.insertSublayer(cameraManager.previewLayer, at: 0)
        cameraManager.setupCameraCaptureSession()
        cameraManager.startPreview()
    }
    
    private func setupSpeechRecognition() {
        let speechController = AppDelegate.sharedSpeechController
        guard speechController.isAuthorized() else {
            return
        }
        speechController.delegate = self
    }
    
    private func startCameraCapture() {
        let cameraManager = AppDelegate.sharedCameraManager
        cameraManager.startCapture()
    }
    
    private func startSpeechCapture() {
        let speechController = AppDelegate.sharedSpeechController
        guard speechController.isAuthorized() else {
            return
        }
        speechController.startCapture { (error, task) in
            if let error = error {
                Debug.log(error: error)
                return
            }
        }
    }
}

extension ViewController : SpeechControllerDelegate {
    func speechControllerDidReceiveSpeechTranscriptionOutput(transcription: SpeechController.SpeechTranscription) {
        transcriptionViewController.setTranscriptionText(transcription.formattedString)
        reviewViewController.setTranscriptionText(transcription.formattedString)
    }
    
    func speechControllerDidBecomeAvailable() {
        
    }
    
    func speechControllerDidBecomeUnavailable() {
        
    }
}

extension ViewController : CaptureButtonViewControllerDelegate {
    func captureButtonDidBeginHoldDown() {
        Debug.log(message: "Starting voice capture")
        startCameraCapture()
        startSpeechCapture()
    }
    
    func captureButtonDidReleaseHoldDown() {
        Debug.log(message: "Stopping voice capture")
        let speechController = AppDelegate.sharedSpeechController
        guard speechController.isCapturing() else {
            return
        }
        speechController.stopCapture()
        let cameraManager = AppDelegate.sharedCameraManager
        cameraManager.stopCapture()
        present(reviewViewController, animated: true) {
            self.reviewViewController.onRequestDismiss = {
                cameraManager.startPreview()
                self.reviewViewController.dismiss(animated: true)
            }
        }
    }
}

extension ViewController : CameraManagerDelegate {
    func cameraManagerDidBeginFileOutput(toFileURL fileURL: URL) {
        // TODO
    }
    
    func cameraManagerDidFinishFileOutput(toFileURL fileURL: URL) {
        reviewViewController.setVideoFileURL(fileURL)
    }
    
    func cameraManagerDidReceiveCameraDataOutput(videoData: CMSampleBuffer) {
        // TODO
    }
}
