import UIKit

protocol CaptureButtonViewControllerDelegate {
    func captureButtonDidBeginHoldDown()
    func captureButtonDidReleaseHoldDown()
}

class CaptureButtonViewController : UIViewController {
    
    private var captureButton: UIButton!
    private var captureButtonCenterView: UIView!
    private var captureButtonBorderView: UIView!
    
    public var delegate: CaptureButtonViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureButton()
    }
    
    private func setupCaptureButton() {
        let bottomOffset: Double = 50
        let width: Double = 45
        let height: Double = width
        let x = Double(view.frame.midX) - width / 2
        let y = Double(view.frame.height) - height - bottomOffset
        captureButton = UIButton(frame: CGRect(x: x, y: y, width: width, height: height))
        captureButton.setTitle("", for: .normal)
        captureButton.addTarget(self, action: #selector(onCaptureButtonDidBeginHoldDown), for: .touchDown)
        captureButton.addTarget(self, action: #selector(onCaptureButtonDidReleaseHoldDown), for: .touchUpInside)
        
        // Center
        captureButtonCenterView = UIView(frame: captureButton.bounds)
        captureButtonCenterView.backgroundColor = ThemeColors.white
        captureButtonCenterView.layer.cornerRadius = CGFloat(width / 2)
        captureButtonCenterView.isUserInteractionEnabled = false
        captureButton.insertSubview(captureButtonCenterView, at: 1)
        
        // Border
        let borderThickness: Double = 15
        let borderWidth = width + borderThickness * 2
        let borderHeight = borderWidth
        captureButtonBorderView = UIView(frame: CGRect(x: -borderThickness, y: -borderThickness, width: borderWidth, height: borderHeight))
        captureButtonBorderView.layer.cornerRadius = CGFloat(borderWidth / 2)
        captureButtonBorderView.backgroundColor = UIColor.white.withAlphaComponent(0)
        captureButton.addSubview(captureButtonBorderView)
        captureButtonBorderView.isUserInteractionEnabled = false
        let effect = UIBlurEffect(style: .light)
        let effectView = UIVisualEffectView(effect: effect)
        effectView.frame = captureButtonBorderView.bounds
        captureButtonBorderView.insertSubview(effectView, at: 0)
        captureButtonBorderView.layer.masksToBounds = true
        captureButton.insertSubview(captureButtonBorderView, at: 0)
        view.addSubview(captureButton)
    }
    
    @objc
    private func onCaptureButtonDidBeginHoldDown(sender: UIButton) {
        delegate?.captureButtonDidBeginHoldDown()
        UIButton.animate(withDuration: 0.3) {
            self.captureButton.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
            self.captureButtonBorderView.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
            self.captureButtonBorderView.backgroundColor = ThemeColors.white.withAlphaComponent(0.35)
        }
    }
    
    @objc
    private func onCaptureButtonDidReleaseHoldDown(sender: UIButton) {
        delegate?.captureButtonDidReleaseHoldDown()
        UIButton.animate(withDuration: 0.3) {
            self.captureButton.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.captureButton.backgroundColor = ThemeColors.white
            self.captureButtonBorderView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.captureButtonBorderView.backgroundColor = ThemeColors.white.withAlphaComponent(0.5)
        }
    }
}
