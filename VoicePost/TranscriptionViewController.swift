import UIKit

class TranscriptionViewController : UIViewController {
    
    private var containerView: UIView!
    private var backgroundView: UIView!
    private var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let height: CGFloat = 120
        let width: CGFloat = view.frame.width
        let y: CGFloat = 100
        let x: CGFloat = 0
        let frame = CGRect(x: x, y: y, width: width, height: height)
        containerView = UIView(frame: frame)
        view.addSubview(containerView)
        
        let containerPadding: CGFloat = 10
        backgroundView = UIView(frame: CGRect(
            x: containerPadding,
            y: containerPadding,
            width: containerView.frame.width - containerPadding * 2,
            height: containerView.frame.height - containerPadding * 2))
        containerView.addSubview(backgroundView)
        
        let insidePadding: CGFloat = 10
        label = UILabel(frame: CGRect(
            x: insidePadding,
            y: insidePadding,
            width: backgroundView.frame.width - insidePadding * 2,
            height: backgroundView.frame.height - insidePadding * 2))
        label.textColor = .white
        label.numberOfLines = 3
        label.textAlignment = .left
        label.font = label.font.withSize(20)
        backgroundView.addSubview(label)
    }
    
    public func setTranscriptionText(_ transcriptionText: String) {
        label.text = transcriptionText
    }
}
