import UIKit

class ReviewViewController : UIViewController {
    
    private var label: UILabel!
    private var exportButton: UIButton!
    private var text: String = "" // TODO should be optional
    private var videoFileURL: URL?
    
    public var onRequestDismiss: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupExportButton()
        setupLabel()
        view.backgroundColor = .white
    }
    
    public func setTranscriptionText(_ transcriptionText: String) {
        text = transcriptionText
    }
    
    public func setVideoFileURL(_ url: URL) {
        videoFileURL = url
    }
    
    private func setupLabel() {
        let padding: CGFloat = 20
        let width: CGFloat = view.frame.width + padding * 2
        let height: CGFloat = view.frame.height + padding * 2
        label = UILabel(frame: CGRect(x: padding, y: padding, width: width, height: height))
        label.textColor = ThemeColors.darkPurple
        label.text = text
        view.addSubview(label)
    }
    
    private func setupExportButton() {
        let width: CGFloat = 100
        let height: CGFloat = 50
        let offsetButtom: CGFloat = 100
        let x: CGFloat = view.frame.midX - width / 2
        exportButton = UIButton(frame: CGRect(x: x, y: view.frame.height - height - offsetButtom, width: width, height: height))
        exportButton.addTarget(self, action: #selector(onExportButtonDidReceiveTouchUpInside), for: .touchUpInside)
        exportButton.setTitle("Save", for: .normal)
        exportButton.setTitleColor(ThemeColors.white, for: .normal)
        exportButton.backgroundColor = ThemeColors.darkPurple
        exportButton.layer.cornerRadius = 10
        view.addSubview(exportButton)
    }
    
    @objc
    private func onExportButtonDidReceiveTouchUpInside() {
        exportVideo()
        onRequestDismiss?()
    }
    
    private func exportVideo() {
        guard let videoFileURL = videoFileURL else {
            return
        }
        do {
            // TODO: set exportButton to disabled
            try VideoAnimation(withVideoAtURL: videoFileURL)?
                .addTextOverlay(withString: text)
                .exportVideo() { error in
                    if let error = error {
                        // TODO: handle error
                        return
                    }
                    // TODO: set exportButton to enabled
            }
        }
        catch let error {
            Debug.log(error: error)
        }
    }
}
